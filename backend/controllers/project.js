'use strict'

var Project = require('../models/project');
var fs = require('fs');

var controller = {
    home: function (req, res) {
        return res.status(200).send({
            message: 'Desde home'
        });
    },

    test: function (req, res) {
        return res.status(200).send({
            message: 'Desde test'
        });
    },

    saveProject : function (req, res) {
        var project = new Project();

        var params = req.query;
        project.name = params.name;
        project.description = params.description;
        project.category = params.category;
        project.year = params.year;
        project.langs = params.langs;
        project.image = null;

        project.save((err, projectStored) => {
            if (err) return res.status(500).send({message: 'Error al guardar', err: err});

            if (!projectStored) return res.status(404).send({message: 'No se ha podido guardar'});

            return res.status(200).send({project: projectStored});
        })
    },

    getProject: function (req, res) {
        var projectId = req.params.id;

        if (projectId == null) return res.status(404).send({message: 'El projecto no existe'});

        Project.findById(projectId, (err, project) => {
            if (err) return res.status(500).send({message: 'Error al listar datos'});

            if (!project) return res.status(404).send({message: 'El projecto no existe'});

            return res.status(200).send({project});
        })
    },

    getProjects: function(req, res) {

        Project.find({}).sort('+year').exec((err, projects) => {
            if (err) return res.status(500).send({message: 'Error al listar datos'});

            if (!projects) return res.status(404).send({message: 'No existen projectos'});

            return res.status(200).send({projects});
        });
        
    },

    updateProject: function (req, res) {
        
        var projectId = req.params.id;
        var update = req.body;

        Project.findByIdAndUpdate(projectId, update, {new: true}, (err, projectUpdate) => {

            if (err) return res.status(500).send({message: 'Error al acualizar'});

            if (!projectUpdate) return res.status(404).send({message: 'No existen projecto para actualizar'});

            return res.status(200).send({
                project: projectUpdate
            });
        });
        
    },

    deleteProject: function (req, res) {

        var projectId = req.params.id;

        Project.findByIdAndRemove(projectId, (err, projectDelete) => {
            if (err) return res.status(500).send({message: 'Error al eliminar'});

            if (!projectDelete) return res.status(404).send({message: 'No existen projecto para eliminar'});

            return res.status(200).send({
                project: projectDelete
            });
        });
    },

    uploadImage: function (req, res) {
        var projectId = req.params.id;
        var fileName = 'no subida..';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif') {

                Project.findByIdAndUpdate(projectId, {image: fileName}, {new: true}, (err, projectUpdate) => {
                    if (err) return res.status(500).send({message: 'La img no se subio'});
    
                    if (!projectUpdate) return res.status(404).send({message: 'No existe el proyecto'});
                }); 
            }else{
                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'la extension no es valida'});
                });
            }

            return res.status(200).send({
                project: req.files
            });
        }

    }
};

module.exports = controller;