'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3700;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/portafolio')
    .then(() => {
        console.log("Conectado con exito");

        //creacion de servidor
        app.listen(port, () => {
            console.log("servidor corriendo");
        })
    })
    .catch(err => console.log(err));